package pkg;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;

public class VideoStreamer {

	public static void main(String[] args) {
	      System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		  VideoCapture capture = new VideoCapture(0);
		  
	      long oldTime=0;
	      long newTime=1;
	      int fps = 0;
	      int millis = 0;
	      Mat framein = new Mat(480, 640, CvType.CV_8UC1);
	      URL url;
	      HttpURLConnection connection;
	      
	      try {
	    	  url = new URL("http://localhost/RoboticsAPI/tennis/status");
	    	  connection = (HttpURLConnection) url.openConnection();
	    	  connection.setDoOutput(true);
	    	  connection.setRequestMethod("GET");
	    	  //OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
	    	  //writer.write("aw, hell");
	    	  //writer.close();
	    	  System.out.println(connection.getResponseMessage());
	      } catch (MalformedURLException e) {
	    	  e.printStackTrace();
	      } catch (IOException e) {
	    	  e.printStackTrace();
	      }
	      
	      while(true) {
	    	  oldTime = System.nanoTime();
	    	  capture.read(framein);
	    	  try {
		    	  url = new URL("http://localhost/RoboticsAPI/tennis/distancecenter");
		    	  connection = (HttpURLConnection) url.openConnection();
		    	  connection.setDoOutput(true);
		    	  connection.setRequestMethod("POST");
		    	  connection.setRequestProperty("Content-Type", "image/jpeg");
		    	  OutputStream out = connection.getOutputStream();
		    	  MatOfByte buffer = new MatOfByte();
		    	  Imgcodecs.imencode(".bmp", framein, buffer);
		    	  out.write(buffer.toArray());
		    	  out.close();
		    	  //System.out.println(connection.getResponseMessage());
		      } catch (MalformedURLException e) {
		    	  e.printStackTrace();
		      } catch (IOException e) {
		    	  e.printStackTrace();
		      }
	    	  newTime = System.nanoTime();
			  fps=(int)(1000000000.0/(newTime-oldTime));
			  millis = (int) ((newTime-oldTime)/1000000.0);
			  //System.out.println(millis);
	      }
	}

}
