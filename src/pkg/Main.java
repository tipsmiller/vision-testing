package pkg;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.*;
import java.awt.*;
import java.awt.image.*;
//import java.util.StringTokenizer;
import javax.swing.*;
//import org.opencv.video.*;
import org.opencv.videoio.VideoCapture;

public class Main {
	
  public static JSlider hLow = new JSlider(JSlider.HORIZONTAL, 0, 180, 5);
  public static JSlider hHigh = new JSlider(JSlider.HORIZONTAL, 0, 180, 5);
  public static JSlider sLow = new JSlider(JSlider.HORIZONTAL, 0, 255, 5);
  public static JSlider sHigh = new JSlider(JSlider.HORIZONTAL, 0, 255, 5);
  public static JSlider vLow = new JSlider(JSlider.HORIZONTAL, 0, 255, 5);
  public static JSlider vHigh = new JSlider(JSlider.HORIZONTAL, 0, 255, 5);
  
  public static void main( String[] args )
  {
      System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
      Mat mat = Mat.eye( 3, 3, CvType.CV_8UC1 );
      System.out.println( "mat = " + mat.dump() );
      
      
      addSliderFrame();
      //setup preview windows
      JLabel output1 = makeWindow("Output1", 700, 525, 700, 0);
      
      long oldTime=0;
      long newTime=1;
      int fps = 0;
      
      Mat framein = new Mat(480, 640, CvType.CV_8UC1);
      Mat framegray = new Mat(480, 640, CvType.CV_8UC1);
      Mat circles1 = new Mat(480, 640, CvType.CV_8UC1);
      Mat framecvt = new Mat(480, 640, CvType.CV_8UC1);
      Mat frameout = new Mat(480, 640, CvType.CV_8UC1);
	  VideoCapture capture = new VideoCapture(0);
      
      int posX = -1;
      int posY = -1;
      long area = 0;
      //int correctionX = 0;
      //int correctionY = 0;
      boolean leftRight = false; //false=left, true=right
      boolean scanning = false;
      int actualX = 1600;
      int actualY = 1425;
      String outString = "";
      int inByte;
      long lockCount = 0;
      int scanCount = 0;
      Moments mo = new Moments();
      
	  while(true){
		  oldTime = System.nanoTime();
		  capture.read(framein);
		  Imgproc.cvtColor(framein, framecvt, Imgproc.COLOR_RGB2HSV);
		  Core.inRange(framecvt, new Scalar(hLow.getValue(), sLow.getValue(), vLow.getValue()), new Scalar(hHigh.getValue(), sHigh.getValue(), vHigh.getValue()), framegray);
		  mo = Imgproc.moments(framegray); // m10/area=x, m01/area=y, m00=area
		  area = (long)mo.get_m00();
		  posX = 0;
		  posY = 0;
		  if(area>100000)
		  {
			  posX = (int)(mo.get_m10()/area);
			  posY = (int)(mo.get_m01()/area);
			  scanCount = 50;
		  }
		  else
		  {
			  scanning = true;
			  if(scanCount > 60)
			  {
				  scanCount = 0;
			  }
			  scanCount++;
			  if(leftRight == false)
			  {
				  if(actualX > 2400)
				  {
					  leftRight = true;
					  actualX += -5;
				  }
				  else
				  {
					  actualX += 5;
				  }
			  }
			  else if(leftRight == true){
				  if(actualX < 800)
				  {
					  leftRight = false;
					  actualX += 5;
				  }
				  else
				  {
					  actualX += -5;
				  }
			  }
			  actualY = 1425;
		  }

		  if(posX != 0 && posY != 0)//BEGIN motor duration set block
		  {
			  actualX += (posX-320)/13;
			  actualY += -(posY-240)/11;
		  }
		  outString = "X";
		  if(actualX > 2400){actualX = 2400;}
		  if(actualX < 800){actualX = 800;}
		  if(actualY > 1700){actualY = 1700;}
		  if(actualY < 1150){actualY = 1150;}
		  if(actualX < 1000)
		  {
			  outString += "0" + actualX;
		  }
		  else
		  {
			  outString += actualX;
		  }
		  if(actualY < 1000)
		  {
			  outString += "0" + actualY;
		  }
		  else
		  {
			  outString += actualY;
		  }//END motor duration set block
		  //BEGIN lock-on block
		  if((posX > 290 && posX < 350) && (posY > 220 && posY < 260))
		  {
			  lockCount ++;
			  if(lockCount == 10)
			  {
				  //Locked On!!!
				  outString += "1";
			  }
			  else if (lockCount > 10)
			  {
				  outString += "1";
			  }
			  else
			  {
				  outString += "0";
			  }
		  }
		  else 
		  {
			  lockCount = 0;
			  outString += "0";
		  }//END lock-on block
		  
					  
		  Core.add(framegray, circles1, frameout);
		  Imgproc.line(frameout, new Point(0, 240), new Point(640, 240), new Scalar(255,255,255));
		  Imgproc.line(frameout, new Point(320, 0), new Point(320, 480), new Scalar(255,255,255));
		  addFrameText(frameout, "Area: " + Double.toString(area), 10, 20);
		  addFrameText(frameout, "FPS: " + Integer.toString(fps), 10, 40);
		  addFrameText(frameout, "hLow: " + hLow.getValue(), 10, 60);
		  addFrameText(frameout, "hHigh: " + hHigh.getValue(), 10, 80);
		  addFrameText(frameout, "sLow: " + sLow.getValue(), 10, 100);
		  addFrameText(frameout, "sHigh: " + sHigh.getValue(), 10, 120);
		  addFrameText(frameout, "vLow: " + vLow.getValue(), 10, 140);
		  addFrameText(frameout, "vHigh: " + vHigh.getValue(), 10, 160);
		  addFrameText(frameout, "posX: " + posX, 10, 180);
		  addFrameText(frameout, "posY: " + posY, 10, 200);
		  addFrameText(frameout, "outString: " + outString, 10, 220);
		  displayFrame(frameout, output1);
		  
		  newTime = System.nanoTime();
		  fps=(int)(1000000000.0/(newTime-oldTime));
		  //System.out.println("FPS: " + fps + ", x: " + posX + ", y: " + posY + ", area: " + mo.get_m00());
	  }
   }
   
   public static Image toBufferedImage(Mat m){
	      int type = BufferedImage.TYPE_BYTE_GRAY;
	      if ( m.channels() > 1 ) {
	          type = BufferedImage.TYPE_3BYTE_BGR;
	      }
	      int bufferSize = m.channels()*m.cols()*m.rows();
	      byte [] b = new byte[bufferSize];
	      m.get(0,0,b); // get all the pixels
	      BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
	      final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
	      System.arraycopy(b, 0, targetPixels, 0, b.length);
	      return image;
	  }
  
  public static void displayFrame(Mat frame, JLabel panel)
  {
	  panel.setIcon(new ImageIcon(toBufferedImage(frame)));
	  panel.repaint();
  }
  
  public static JLabel makeWindow(String name, int sizex, int sizey, int posx, int posy)
  {
	  JFrame jframe=new JFrame(name);
	  jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  JLabel label = new JLabel();
	  jframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
	  jframe.setUndecorated(true);
	  jframe.setContentPane(label);
	  //jframe.setSize(sizex, sizey);
	  //jframe.setLocation(posx, posy);
	  jframe.setVisible(true);
	  
	  return label;
  }
  
  public static void addFrameText(Mat frame, String text, int x, int y)
  {
	  Imgproc.putText(frame,  text,  new Point(x,y), 0, .5, new Scalar(255,255,255));
  }
  
  public static void addSliderFrame()
  {
	  JFrame sliderFrame=new JFrame("sliderFrame");
	  JPanel sliderPanel = new JPanel();
	  sliderPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	  sliderFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  sliderFrame.setVisible(true);
	  sliderFrame.setSize(400, 400);
	  hLow.setValue(56); //Slider defaults
	  hHigh.setValue(75);
	  sLow.setValue(119);
	  sHigh.setValue(223);
	  vLow.setValue(130);
	  vHigh.setValue(234);
	  sliderPanel.add(hLow);
	  sliderPanel.add(hHigh);
	  sliderPanel.add(sLow);
	  sliderPanel.add(sHigh);
	  sliderPanel.add(vLow);
	  sliderPanel.add(vHigh);
	  sliderFrame.add(sliderPanel);
  }
}
